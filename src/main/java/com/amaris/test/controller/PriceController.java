package com.amaris.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.amaris.test.controller.entity.PriceRequest;
import com.amaris.test.controller.entity.PriceResponse;
import com.amaris.test.service.PriceService;

@Controller
public class PriceController {

    @Autowired
    private PriceService priceService;

    @RequestMapping(value = "/price" ,method = RequestMethod.POST)
    public ResponseEntity<PriceResponse> getPrice(@RequestBody PriceRequest priceRequest) {
        PriceResponse priceResponse = priceService.getPrice(priceRequest);
        if (priceResponse != null) {
            return ResponseEntity.ok(priceResponse);
        } else {
            return new ResponseEntity<PriceResponse>(HttpStatus.NO_CONTENT);
        }
    }

}
