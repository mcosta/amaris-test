package com.amaris.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmarisJava {

	public static void main(String[] args) {
		SpringApplication.run(AmarisJava.class, args);
	}

}
